name "redis-config"
maintainer       "Par de Dois"
maintainer_email "postmaster@pardedois.com"
license          "Apache 2.0"
description      "Configures Redis With ElasticCache"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.1"

recipe "redis-config", "COnfigure Redis with ElasticCache"
recipe "redis-config", "Generate the config file"
